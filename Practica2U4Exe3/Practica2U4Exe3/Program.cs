﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Practica2U4Exe3
{
    class Program
    {
        static void Main(string[] args)
        {
            MonitoreoDelViaje MV = new MonitoreoDelViaje();
            AccionRealizada AR = new AccionRealizada();
            Console.WriteLine("nro_monitoreo: ");
            AR.nro_Monitoreo =int.Parse(Console.ReadLine());
            Console.WriteLine("Diagnostico_pac: ");
            AR.Diagnostico_pac = Console.ReadLine();
            Console.WriteLine("hora: ");
            MV.Hora = int.Parse(Console.ReadLine());
            Console.WriteLine("Pulso: ");
            MV.pulso = int.Parse(Console.ReadLine());
            Console.WriteLine("Lat_fetales: ");
            MV.lat_fetales = int.Parse(Console.ReadLine());
            Console.WriteLine("nro_contracciones: ");
            MV.nro_contracciones = int.Parse(Console.ReadLine());
            Console.WriteLine("Hem_cantidad: ");
            MV.hem_cantidad = int.Parse(Console.ReadLine());
            Console.WriteLine("convulsiones_hora: ");
            MV.convulsiones_Hora = int.Parse(Console.ReadLine());
            Console.WriteLine("Paro_cardio: ");
            MV.paro_cardio = Console.ReadLine();
            Console.WriteLine("Otros: ");
            MV.otros = Console.ReadLine();
            Console.WriteLine("cod_accion: ");
            AR.cod_accion = Console.ReadLine();
            Console.WriteLine("Descripcion: ");
            AR.descripcion = Console.ReadLine();
            Console.WriteLine("Tipo: ");
            AR.Tipo = Console.ReadLine();

            Console.ReadKey();

        }
    }
}
