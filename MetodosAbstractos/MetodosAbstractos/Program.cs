﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MetodosAbstractos
{
    class Program
    {
        static void Main(string[] args)
        {
            
            Automovil AM = new Automovil();
            Avion AV = new Avion();
            AM.Mantenimiento();
            AV.Mantenimiento();

            Console.ReadKey();
        }
    }
}
