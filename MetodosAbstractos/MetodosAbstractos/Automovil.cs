﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MetodosAbstractos
{
    class Automovil:Transporte
    {
        public override void Mantenimiento()
        {
            Console.WriteLine("Listado de mantenimiento del Automovil-*\n-Cambio de acetite-*\n-Cambio de bujia-*\n-Chequeo de llantas-*\n-Liquido de los frenos-*\n-Cambio de filtros ");
        }
    }
}
