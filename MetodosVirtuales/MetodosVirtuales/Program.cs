﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MetodosVirtuales
{
    class Program
    {
        static void Main(string[] args)
        {
            Triangulo Tri = new Triangulo();
            Tri.CalcularArea(5,7);
            Cuadrado Cua = new Cuadrado();
            Cua.CalcularArea(5,8);
            Rectangulo Rec = new Rectangulo();
            Rec.CalcularArea(10,8.5);

            Console.ReadKey();
        }
    }
}
