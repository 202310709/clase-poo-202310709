﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MetodosVirtuales
{
    class Triangulo
    {
        public virtual void CalcularArea(double B, double A)
        {
            double resultado = (B * A )/ 2;
            Console.WriteLine("El resultado del area del triangulo es: " + resultado);
        }
    }
}
