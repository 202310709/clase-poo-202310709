﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MetodosVirtuales
{
    class Rectangulo:Cuadrado
    {
        public override void CalcularArea(double B, double A)
        {
            double resultado = B * A;
            Console.WriteLine("El area del rectangulo es: "+resultado);
        }
    }
}
