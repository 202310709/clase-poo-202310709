﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MetodosVirtuales
{
    class Cuadrado:Triangulo
    {
        public override void CalcularArea(double B, double A)
        {
            double resultado = B * A;
            Console.WriteLine("El area del cuadrado es: "+resultado);
        }
    }
}
