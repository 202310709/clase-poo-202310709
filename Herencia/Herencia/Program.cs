﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Herencia
{
    class Program
    {
        static void Main(string[] args)
        {
            
            Hijo H = new Hijo();
            H.metodo1();
            H.accesometodo3();
            Herencia He = new Herencia();
            He.accesometodo2();

            Console.ReadKey();
        }
    }
}
