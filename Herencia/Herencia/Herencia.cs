﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Herencia
{
    class Herencia
    {
        public int atributo1;
        private int atributo2;
        protected int atributo3;


        public void metodo1()
        {
            Console.WriteLine("Este es el metodo 1 de la herencia");
        }
        private void metodo2()
        {
            Console.WriteLine("Este metodo no se puede acceder fuera de la clase herencia");
        }
        protected void metodo3()
        {
            Console.WriteLine("Este es el metodo 3 de la herencia protected");

        }
        public void accesometodo2()
        {
            metodo2();
        }
        
    }
    class Hijo : Herencia
    {
        public void accesometodo3()
        {
            metodo3();
        }
    }
}
