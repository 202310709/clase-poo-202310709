﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExamenU3
{
    class Operaciones
    {
        double A, B;
        //Constructor
        public Operaciones(double a, double b)
        {
            A = a;
            B = b;
        }
        public double Suma()
        {
            return A + B;
        }
        private double Resta()
        {
            return A - B;
        }
        public double AccesoResta()
        {
            Resta();
            return A - B;
        }
        protected double multiplicar()
        {
            return A * B;
        }
        public double AccesoMultiplicar()
        {
            multiplicar();
            return A * B;
        }
        ~Operaciones()
        {
            //Destructor
        }
    }
    
}

