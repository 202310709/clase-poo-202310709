﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExamenU3
{
    class Program
    {
        static void Main(string[] args)
        {
            Operaciones O = new Operaciones(5,10);
            
            var resta = O.AccesoResta();
            var multiplicacion = O.AccesoMultiplicar();
            HerenciaOperaciones HO = new HerenciaOperaciones(5,10);
            var suma = HO.AccesoMetodoSuma();

            Console.WriteLine("La suma es " + HO.AccesoMetodoSuma());
            Console.WriteLine("La resta es " + O.AccesoResta());
            Console.WriteLine("La multiplicacion es " + O.AccesoMultiplicar());

            Console.ReadKey();

        }
    }
}
