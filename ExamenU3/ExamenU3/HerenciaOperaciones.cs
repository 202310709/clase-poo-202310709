﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExamenU3
{
    class HerenciaOperaciones:Operaciones
    {
        double A, B;
        //Constructor
        public HerenciaOperaciones(double a, double b) : base(a, b)
        {
            A = a;
            B = b;
        }
        public double AccesoMetodoSuma()
        {
            base.Suma();
            return A + B;
        }
        //Destructor
        ~HerenciaOperaciones()
        {

        }
    }
    
}
