﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExamenU4Exe1
{
    class Program
    {
        static void Main(string[] args)
        {
            Padre P = new Padre();
            int resultado= P.Numero(10,4);
            Console.WriteLine("Resultado de metodo 1: "+resultado);
            double resultado2=P.Numero(10.5,4.8);
            Console.WriteLine("Resultado de metodo 2: "+resultado2);
            Console.ReadKey();
        }
    }
}
