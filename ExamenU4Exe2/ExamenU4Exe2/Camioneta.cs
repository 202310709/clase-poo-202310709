﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExamenU4Exe2
{
    class Camioneta:Automovil
    {
        public override void CalculaRenta(int dias, double costo)
        {
            CostoRenta = dias * costo;
        }
    }
}
