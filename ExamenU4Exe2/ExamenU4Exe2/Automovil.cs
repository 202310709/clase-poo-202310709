﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExamenU4Exe2
{
     abstract class Automovil
    {
        public double CostoRenta;
        public abstract void CalculaRenta(int dias, double costo);
    }
   
}
