﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Practica_01_U2
{
    class Operacion
    {
        public int suma = 0;
        public void ImprimePares(int n)
        {
            for (int i = 1; i <= n; i++)
            {
                if (i % 2 == 0)
                {
                    Console.WriteLine(i);
                    suma = suma + i;
                }
            }
        }
        public void SumaArreglo(int n)
        {
            int[] a = new int[n];
            for (int i = 0; i < n; i++)
            {
                suma = suma + a[i];
            }
        }
        public int Suma
        {
            get { return suma; }
            set { suma = value; }
        }

    }
      
}
