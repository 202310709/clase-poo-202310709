﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Practica_01_U2
{
    class Program
    {
        static void Main(string[] args)
        {
            int n;
            Operacion O = new Operacion();
            Console.Write("Ingresa el valor de n: ");
            n = int.Parse(Console.ReadLine());
            O.ImprimePares(n);
            O.SumaArreglo(n);
            var CopiaSuma = O.suma;
            Console.WriteLine("Suma del arreglo = " + CopiaSuma);
            Console.ReadKey();
        }
       
    }
    
}
