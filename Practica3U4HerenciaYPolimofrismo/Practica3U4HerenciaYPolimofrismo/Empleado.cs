﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Practica3U4HerenciaYPolimorfismo
{
    class Empleado
    {
        double horas_Trabajadas;
        public double Horas_Trabajadas()
        {
            Console.WriteLine("Cuantas horas trabajaste: ");
            horas_Trabajadas = double.Parse(Console.ReadLine());
            return horas_Trabajadas;
        }


    }
    class Intendentes:Empleado
    {

    }
    class Produccion:Empleado
    {

    }
    class Administrativos:Empleado
    {

    }
    class Gerencias:Empleado
    {

    }
    class Dueños:Empleado
    {


    }


}
