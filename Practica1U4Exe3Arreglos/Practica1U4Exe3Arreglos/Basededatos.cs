﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Practica1U4Exe3Arreglos
{
    class Basededatos
    {
        public void LlenarDatos(String[] n)
        {
            for (int i = 0; i < 35; i++)
            {
                Console.WriteLine("ingresa nombre " + (i + 1) + ":");
                n[i] = Console.ReadLine();
            }
        }
        public void LlenarDatos(int[] t)
        {
            for (int j = 0; j < 35; j++)
            {
                Console.WriteLine("ingresa telefono " + (j + 1) + ":");
                t[j] = int.Parse(Console.ReadLine());
            }
        }
        public void imprimirdatos(String[] nombres, int[] telefonos)
        {
            for (int i = 0; i < 35; i++)
            {
                Console.Write("nombre: " + nombres[i] + " tel: " + telefonos[i]);
                Console.WriteLine("");
            }
        }

    }
}
