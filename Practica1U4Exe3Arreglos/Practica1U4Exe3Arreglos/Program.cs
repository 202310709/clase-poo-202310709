﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Practica1U4Exe3Arreglos
{
    class Program
    {
        static void Main(string[] args)
        {
            String[] nombres = new String[35];
            int[] telefonos = new int[35];


            Basededatos BD = new Basededatos();
            BD.LlenarDatos(nombres);
            BD.LlenarDatos(telefonos);
            BD.imprimirdatos(nombres, telefonos);

            Console.ReadKey();
        } 
    }
}
