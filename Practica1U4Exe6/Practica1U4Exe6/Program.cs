﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Practica1U4Exe6
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] n = new int[300];
            Clase c = new Clase();
            c.contador(n);
            c.zero(n);
            c.positivo(n);
            c.negativo(n);
            Console.WriteLine("ceros totales: {0}", c.ceros);
            Console.WriteLine("negativos totales: {0}", c.negativos);
            Console.WriteLine("positivos totales: {0}", c.positivos);
            Console.WriteLine("suma total de los numeros negativos: {0}", c.suman);
            Console.WriteLine("suma total de los numeros positivos: {0}", c.sumap);


            Console.ReadKey();
        }
    }
}
