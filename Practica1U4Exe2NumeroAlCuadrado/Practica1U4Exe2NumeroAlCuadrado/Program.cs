﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Practica1U4Exe2NumeroAlCuadrado
{
    class Program
    {
        static void Main(string[] args)
        {
            //Tipo de dato int
            int Num1;
            Console.WriteLine("Ingresa el numero que se elevara al cuadrado : ");
            Num1 = int.Parse(Console.ReadLine());
            Console.WriteLine("El cuadrado del numero " +Num1 + " es :"+Num1*Num1);
            //Tipo de dato double
            double Num2;
            Console.WriteLine("Ingresa el numero que se elevara al cuadrado : ");
            Num2= double.Parse(Console.ReadLine());
            Console.WriteLine("El cuadrado del numero " + Num2 + " es :" + Num2 * Num2);
            //Tipo de dato float
            float Num3;
            Console.WriteLine("Ingresa el numero que se elevara al cuadrado : ");
            Num3= float.Parse(Console.ReadLine());
            Console.WriteLine("El cuadrado del numero " + Num3 + " es :" + Num3 * Num3);

            Console.ReadKey();
        }
    }
}
