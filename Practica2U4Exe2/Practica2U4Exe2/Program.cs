﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Practica2U4Exe2
{
    class Program
    {
        static void Main(string[] args)
        {
            
            Flight F = new Flight();
            Console.WriteLine("Reservation: ");
            F.Reservation_Availablity = bool.Parse(Console.ReadLine());
            Console.WriteLine("Destination: ");
            F.Destination = char.Parse(Console.ReadLine());
            Console.WriteLine("Departure: ");
            F.Departure = char.Parse(Console.ReadLine());
            Console.WriteLine("Date: ");
            F.Flight_Date = char.Parse(Console.ReadLine());
            Console.WriteLine("Time: ");
            F.Flight_Time = char.Parse(Console.ReadLine());
            Console.WriteLine("Price: "+F.Flight_Price);
            Console.WriteLine("Number: "+F.Flight_Number);
            Console.ReadKey();
        }
    }
}
