﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Practica1U4Exe1Sobrecarga
{
    class Program
    {
        static void Main(string[] args)
        {
            Operacion Op = new Operacion();
            int resultado = Op.Grande(10, 4);
            Console.WriteLine("Resultado de Metodo 1: " +resultado);
            double resultado2 = Op.Grande(15, 35, 23);
            Console.WriteLine("Resultado de Metodo 2: " + resultado2);
            float resultado3 = Op.Grande(10, 12, 12, 18);
            Console.WriteLine("Resultado Metodo 3: " +resultado3);

            Console.ReadKey();
        }
    }
}
