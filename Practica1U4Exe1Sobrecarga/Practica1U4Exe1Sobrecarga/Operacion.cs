﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Practica1U4Exe1Sobrecarga
{
    class Operacion
    {
        public int Grande(int a, int b)
        {
            int resultado = a - b;
            return resultado;
        }
        public double Grande(double a, double b, double c)
        {
            double resultado = a * b * c;
            return resultado;

        }
        public float Grande(float a,float b, float c, float d)
        {
            float resultado = a + b + c + d;
            return resultado;
        }
    }
}
