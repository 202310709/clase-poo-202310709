﻿namespace ExamenPractico
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnCaracterisicas = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txtcolor = new System.Windows.Forms.TextBox();
            this.txttamaño = new System.Windows.Forms.TextBox();
            this.txtTipodelibreta = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.txtNoHojas = new System.Windows.Forms.TextBox();
            this.txtLibretas = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnCaracterisicas
            // 
            this.btnCaracterisicas.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnCaracterisicas.Location = new System.Drawing.Point(338, 369);
            this.btnCaracterisicas.Name = "btnCaracterisicas";
            this.btnCaracterisicas.Size = new System.Drawing.Size(97, 23);
            this.btnCaracterisicas.TabIndex = 0;
            this.btnCaracterisicas.Text = "Caracteristicas";
            this.btnCaracterisicas.UseVisualStyleBackColor = true;
            this.btnCaracterisicas.Click += new System.EventHandler(this.btnCaracterisicas_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(228, 153);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(30, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "color";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(231, 190);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(42, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "tamaño";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(231, 220);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(0, 13);
            this.label3.TabIndex = 3;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(231, 253);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(74, 13);
            this.label4.TabIndex = 4;
            this.label4.Text = "Tipo de libreta";
            // 
            // txtcolor
            // 
            this.txtcolor.Location = new System.Drawing.Point(371, 153);
            this.txtcolor.Name = "txtcolor";
            this.txtcolor.Size = new System.Drawing.Size(145, 20);
            this.txtcolor.TabIndex = 5;
            // 
            // txttamaño
            // 
            this.txttamaño.Location = new System.Drawing.Point(371, 191);
            this.txttamaño.Name = "txttamaño";
            this.txttamaño.Size = new System.Drawing.Size(138, 20);
            this.txttamaño.TabIndex = 6;
            // 
            // txtTipodelibreta
            // 
            this.txtTipodelibreta.Location = new System.Drawing.Point(371, 253);
            this.txtTipodelibreta.Name = "txtTipodelibreta";
            this.txtTipodelibreta.Size = new System.Drawing.Size(138, 20);
            this.txtTipodelibreta.TabIndex = 7;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(197, 20);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(337, 31);
            this.label5.TabIndex = 8;
            this.label5.Text = "Propiedades de la libreta";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(231, 220);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(54, 13);
            this.label6.TabIndex = 9;
            this.label6.Text = "No. Hojas";
            // 
            // txtNoHojas
            // 
            this.txtNoHojas.Location = new System.Drawing.Point(371, 220);
            this.txtNoHojas.Name = "txtNoHojas";
            this.txtNoHojas.Size = new System.Drawing.Size(138, 20);
            this.txtNoHojas.TabIndex = 10;
            // 
            // txtLibretas
            // 
            this.txtLibretas.Location = new System.Drawing.Point(364, 89);
            this.txtLibretas.Name = "txtLibretas";
            this.txtLibretas.Size = new System.Drawing.Size(145, 20);
            this.txtLibretas.TabIndex = 12;
            this.txtLibretas.TextChanged += new System.EventHandler(this.txtLibretas_TextChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(221, 89);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(94, 13);
            this.label7.TabIndex = 11;
            this.label7.Text = "¿Cuantas libretas?";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.txtLibretas);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.txtNoHojas);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.txtTipodelibreta);
            this.Controls.Add(this.txttamaño);
            this.Controls.Add(this.txtcolor);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnCaracterisicas);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnCaracterisicas;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtcolor;
        private System.Windows.Forms.TextBox txttamaño;
        private System.Windows.Forms.TextBox txtTipodelibreta;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtNoHojas;
        private System.Windows.Forms.TextBox txtLibretas;
        private System.Windows.Forms.Label label7;
    }
}

