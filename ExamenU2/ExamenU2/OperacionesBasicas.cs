﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExamenU2
{
    class OperacionesBasicas
    {
        private Double A {get; set;  }
        private Double B { get; set; }


        public OperacionesBasicas(double a, double b )
        {
            A = a;
            B = b;
        }
          public double Suma
         {
            get {
                return A + B;
            }
            set {
                Suma = value;
            }
           
         }
          public double Resta
        {
            get
            {
                return A - B;
            }
            set
            {
                Resta = value;
            }
        }
        

        public double Multiplicacion
          {
            get {
                return A *B;
            }
            set {
                Multiplicacion= value;
            }
        }
           public double Division
          {
            get {
                return A / B;
            }
            set {
                Division = value;
            }
        }

             
    }
}
