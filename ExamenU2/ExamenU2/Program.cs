﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExamenU2
{
    class Program
    {
        static void Main(string[] args)
        {
            OperacionesBasicas O = new OperacionesBasicas(150,60);
            var suma = O.Suma;
            var resta = O.Resta;
            var multiplicacion = O.Multiplicacion;
            var division = O.Division;

            Console.WriteLine("La suma es "+O.Suma);
            Console.WriteLine("La resta es " + O.Resta);
            Console.WriteLine("La multiplicacion es " + O.Multiplicacion);
            Console.WriteLine("La division es " + O.Division);

            Console.ReadKey();


        }
    }
}
