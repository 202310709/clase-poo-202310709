﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Practica2P2
{
    class Program
    {
        static void Main(string[] args)
        {
            Circunferencia Rueda = new Circunferencia();
            Circunferencia Moneda = new Circunferencia();
            Console.WriteLine("Rueda");
            Rueda.CalcularArea(10);
            Rueda.CalcularPerimetro(10);
            Console.WriteLine("Moneda");
            Moneda.CalcularArea(1);
            Moneda.CalcularPerimetro(1);
            Console.ReadKey();
        }
    }
}
