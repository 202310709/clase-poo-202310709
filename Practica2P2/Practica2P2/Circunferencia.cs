﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Practica2P2
{
    class Circunferencia 
    {
        public void CalcularArea(double r)
        {
            double Area = (Math.PI) * (Math.Pow(r, 2));
            Console.WriteLine("El area es: " + Area);
        }
        public void CalcularPerimetro(double r)
        {
            double Perimetro = (Math.PI * 2) * r;
            Console.WriteLine("El perimetro es: " + Perimetro);
        }
    }
}
