﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Practica1U4Exe5VectorShortYLong
{
    class NumeroMayor
    {
        public void rellenar(short[] a)
        {
            short ad = 10000;
            for (int i = 0; i < a.Length; i++)
            {
                Console.WriteLine("ingresa un valor,  posicion({0})", i);
                a[i] = Convert.ToInt16(Console.ReadLine());
            }
        }
        public void rellenar(long[] a)
        {

            for (int i = 0; i < a.Length; i++)
            {
                Console.WriteLine("ingresa un valor, posicion({0})", i);
                a[i] = Convert.ToInt64(Console.ReadLine());
            }
        }
        public void detectarNomayor(short[] a)
        {
            short max = 0;
            int id = 0;
            for (int i = 0; i < a.Length; i++)
            {
                if (a[i] > max)
                {
                    max = a[i];
                    id = i;
                }
            }
            Console.WriteLine("el numero mayor es {0} y esta en la posicion {1}", max, id);
        }
        public void detectarNomayor(long[] a)
        {
            long max = 0;
            int id = 0;
            for (int i = 0; i < a.Length; i++)
            {
                if (a[i] > max)
                {
                    max = a[i];
                    id = i;
                }
            }
            Console.WriteLine("el numero mayor es {0} y esta en la posicion {1}", max, id);
        }
    }
}
