﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Suma_Sobre_Carga_de_metodos
{
    class Program
    {
        static void Main(string[] args)
        {
            Operaciones O = new Operaciones();
            int resultado = O.Suma(5, 9);
            Console.WriteLine("Resultado metodo 1:{0} ", +resultado);
            double resultado2 = O.Suma(5.888854, 9.748465465, 3.14323254);
            Console.WriteLine("Resultado metodo 2: {0}", +resultado2);
            string resultado3 = O.Suma("1", "8");
            float resultado4 = O.Suma(3.9f, 8.9f, 8.9f);
            Console.WriteLine("Resultado metodo 3: {0}", resultado3);
            Console.WriteLine("Resultado metodo 4: {0}", resultado4);

            Console.ReadKey();
        }
    }
}
